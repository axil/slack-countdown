*Cretan cuisine (Κρητική κουζίνα)*

The traditional Cretan cuisine is considered to be one of the healthiest in the world. The abundance and the variety of the products of the island through centuries has created a cuisine with unique taste, freshness and originality.

Today it is believed that olive oil is the secret of the Cretan diet and the Cretan longevity.

The agricultural products of Crete, cucumbers, courgettes and other vegetables are produced in the fertile plains of the island, where it never snows and the temperatures are warm enough for a lunch in the open air even in the heart of winter . These vegetables are grown near the beaches of the south, in optimal conditions. The swallows never leave these places, as the weather is very mild.

Raki or tsikoudia is a local Cretan deistilled alcoholic drink, a clear and fragrant liquor which visitors are offered as a welcoming gesture, or at the end of the meal. If you are offered raki by a Cretan, it's an insult if you don't drink it!

Raki contains about 37% alcohol, similar to Scotch whiskey or gin. It is served straight, in shot glasses, usually well chilled. Every autumn, after the grape harvest, various wine celebrations begin. Then comes the manufacture of raki. The custom of distilleries was instituted by Eleftherios Venizelos in 1920, when special permits were given to farmers to distill raki for additional income. The strafylla (crushed grapes), what is left behind once grapes have been pressed to make wine, are slowly boiled in special cauldrons. Drop by drop, raki begins to flow; very strong at first, almost pure alcohol, and then properly balanced.
