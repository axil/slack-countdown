*Cretan dances (Κρητικοί χοροί)*

Traditional Cretan dances are danced by men and women, who wear their wonderful Cretan costumes in formal events. They are either fast or slow, but always vivid and imposing. When danced by groups of men, they remind of the war dances of Curetes.

With the lyre or the violin as a guide, accompanied by the lute and rarer instruments, such as the bulgari, the askomandoura or the thiamboli, dancers present the complex steps and the first of the cycle, usually a man, performs skillful dance movements called tsalimia.

Each area of the island has its own traditional dances, most of which retain the primitive circular pattern. Main dances are pentozalis, kastrinos, pidichtos, siganos, syrtos and sousta, which is danced in couples and is an erotic and vigorous dance, almost danced on the tip of the toes.

The famous melody of Zorbas is based on an old version of the Cretan dance Syrtos.

Video of <https://www.youtube.com/watch?v=qrzJcbKXmhc|Pentozalis dance>
