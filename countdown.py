#!/usr/bin/env python
from datetime import datetime
import json
import os
import requests

END_DATE = os.environ.get('END_DATE')
SLACK_URL = os.environ.get('SLACK_URL')

if not SLACK_URL:
    print("Missing environment variable SLACK_URL")
    exit(1)

if not END_DATE:
    print("Missing environment variable END_DATE")
    exit(1)

def days_from_date(strdate):
    """ Returns the number of days between strdate and today. Add one to date
    as date caclulate is relative to time
    """
    currentdate = datetime.today()
    futuredate = datetime.strptime(strdate, '%Y-%m-%d')
    delta = futuredate - currentdate
    return delta.days + 1

def date_only(strdate):
    """ Returns string to be displayed. Sends error message if date is
    in the past
    """
    days = days_from_date(strdate)
    assert (days >= -2), "Date needs to be in the future"
    futuredate = datetime.strptime(strdate, '%Y-%m-%d')
    if days == -1:
        return "%d day since %s" % (1, futuredate.strftime("%d %B, %Y"))
    if days == -2:
        return "%d days since %s" % (days, futuredate.strftime("%d %B, %Y"))
    if days == 1:
        return "%d day until %s" % (days, futuredate.strftime("%d %B, %Y"))
    else:
        return "%d days until %s" % (days, futuredate.strftime("%d %B, %Y"))

def post(out):
    """ Posts a request to the slack webhook. Payload can be customized
    so the message in slack is customized. The variable out is the text
    to be displayed.
    """

    days = days_from_date(END_DATE)

    repo_img_dir = "https://gitlab.com/axil/slack-countdown/raw/master/images/"
    image = str(days) + ".jpg"

    tip = str(days) + ".txt"
    f = open("tips/" + tip, 'r')
    tip_msg = f.read()
    f.close()

    payload = {
        "attachments": [
            {
                "title": "GitLab Summit Greece 2017 - " + out + "!",
                "text": "Today's trivia is about " + tip_msg,
                "color": "#7CD197",
                "image_url": repo_img_dir + image,
                "mrkdwn_in": ["text"]
            }
        ]
    }

    requests.post(SLACK_URL, data=json.dumps(payload))

def deadline(date):
    """ Method takes two optional arguments. Displays in slack channel
    the number of days till the event. If no arguments are given,
    the number of days till Christmas is displayed.
    """
    try:
        result = ""
        if date:
            result = date_only(date)
    except:
        print "Error"
    else:
        post(result)

if __name__ == "__main__":
    deadline(END_DATE)
