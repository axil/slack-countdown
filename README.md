Countdown program to Slack hook
-------------------------------

## Background

We built this in the run up to the GitLab Summit 2017. Borrowed from https://github.com/esplorio/slack-countdown

## Setup

1. Create a <a href="https://slack.com/services/new/incoming-webhook" target="_blank"> new incoming webhook</a> for your Slack channel and copy the unique URL. This is the URL countdown.py will be sending post requests to.

2. Export `SLACK_URL`

3. We will need run the countdown.py deadline task. The specification for the method is as follows:
    ```
    The method takes two optional arguments.

    Options:
      -d DEADLINE, --deadline=DEADLINE
                            Specify the deadline in ISO format: yyyy-mm-dd
      -e EVENT, --event=EVENT
                            Name of the deadline event
    ```
    i.e.
    If the date today is the 16th July 2015 then
    - `countdown deadline -d 2015-07-18` will print out “2 days until 18 July 2015”
    - `countdown deadline -d 2015-07-18 -e weekend` will print out “2 days until weekend”.

    If no argument is given, the default is for the method to post how many days till the
    next Christmas.

4. In the terminal type:
    ```
    python countdown.py deadline
    ```
    followed by your desired arguments.

5. Next specify how often you want the script to run, i.e. how often you want a reminder of your deadline in the Slack Channel.

